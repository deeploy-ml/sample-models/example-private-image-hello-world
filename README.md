# Hello world example using a Custom Image

> Based on https://github.com/kubeflow/kfserving/tree/master/docs/samples/v1alpha2/custom/hello-world

The goal of custom image support is to allow users to bring their own wrapped model inside a container and serve it with KFServing. Please note that you will need to ensure that your container is also running a web server e.g. Flask to expose your model endpoints.

In this example we use Docker to build the sample python server into a container. To build and push with Docker Hub, run these commands replacing {username} with your Docker Hub username:

```
# Build the container on your local machine
docker build -t {username}/custom-sample .

# Push the container to docker registry
docker push {username}/custom-sample
```